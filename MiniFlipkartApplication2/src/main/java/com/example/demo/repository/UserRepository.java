package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
	@Query(value="select u.* from user u where u.email=:email",nativeQuery = true)
	public User getUserByEmail(@Param("email") String email);

}

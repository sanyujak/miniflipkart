package com.example.demo.event;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

@Component
public class RegistrationEmailListener implements ApplicationListener<OnRegistrationCompleteEvent>{
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private UserService userService;
	
	

	@Override
	public void onApplicationEvent(OnRegistrationCompleteEvent event) {
		this.confirmRegistration(event);
		
		
	}

	public void confirmRegistration(OnRegistrationCompleteEvent event) {
		User user = event.getUser();
		String token=UUID.randomUUID().toString();
		
		userService.createVerificationToken(user, token);
		
		String receipientAddress=user.getEmail();
		String subject="Registration Confirmation";
		
		SimpleMailMessage email=new SimpleMailMessage();
		email.setTo(receipientAddress);
		email.setSubject(subject);
		email.setText("http://localhost:8080/Shop/regitrationConfirm?token="+token);
		mailSender.send(email);
		
		
	}
}

package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.CustomException;
import com.example.demo.service.UserService;
/**
 * @author Sanyuja Kharat 
 *
 */
@RestController
public class RegistrationController {
	
	@Autowired
	private UserService userService;

	@GetMapping("/regitrationConfirm")
	public String confirmRegistration(@RequestParam("token") String token) throws CustomException {
		
		userService.getVerificationToken(token);
		
		return "redirect:/login.html";
		
		
		
	}
}
